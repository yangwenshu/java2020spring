package java2020spring;

import javax.swing.undo.*;
import java.awt.*;
import java.awt.event.*;
import java.text.*;  
import java.util.*;  
import java.io.*; 
import javax.swing.*;
import javax.swing.border.*; 
import javax.swing.plaf.FileChooserUI;
import javax.swing.text.*;  
import javax.swing.event.*;  
import java.awt.datatransfer.*;  
import java.awt.Font.*; 
public class Mynote extends JFrame implements ActionListener,DocumentListener{
	JTextArea Text;  //文本框
	JMenu fileMenu,editMenu,formatMenu,viewMenu,helpMenu;  //菜单“文件”、“编辑”、“格式”、“视图”、“帮助”
	JMenuItem New,Open,Save,SaveAs,Exit; //文件→新建、打开、保存、另存为、退出
	JMenuItem Copy,Paste,Cut,Undo,Find,Delete;  //编辑→复制、粘贴、剪切、撤销、查找、删除
	JCheckBoxMenuItem LineWrap; //格式→自动换行
	JCheckBoxMenuItem Status; //查看→状态栏
	JMenuItem HelpTopics,AboutNotepad; //帮助→查看帮助、关于
	JPopupMenu popupMenu;   //右键弹出菜单项 
	JMenuItem popupMenu_Undo,popupMenu_Cut,popupMenu_Copy,popupMenu_Paste,popupMenu_Delete;//右键弹出菜单项
	JTextArea editArea;   //“文本”编辑区域 
	Toolkit toolkit=Toolkit.getDefaultToolkit();
	Clipboard clipBoard=toolkit.getSystemClipboard(); //系统剪贴板
	String oldValue;//存放编辑区原来的内容，用于比较文本是否有改动
	boolean isNewFile=true;//是否新文件(未保存过的)  
    File currentFile;//当前文件名  
    JLabel statusLabel; //状态栏标签
    
	public static void main(String args[])   {   
	   Mynote  Mynote=new  Mynote(); 
	   Mynote.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//退出应用程序  
	   }
	 
	public Mynote(){  
		super("我的记事本");
		JMenuBar menuBar=new JMenuBar();   //创建菜单条
//文件
	     //创建“文件”菜单及菜单项并设置快捷键ALT+F 
		fileMenu=new JMenu("文件(F)");  
        fileMenu.setMnemonic('F');  //快捷键ALT+F
            //创建“新建”并注册监视器和快捷键
        New=new JMenuItem("新建(N)");  
        New.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,InputEvent.CTRL_MASK));  
        New.addActionListener(this);  
            //创建“打开”并注册监视器和快捷键
        Open=new JMenuItem("打开(O)...");  
        Open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,InputEvent.CTRL_MASK));  
        Open.addActionListener(this);  
            //创建“保存”并注册监视器和快捷键
        Save=new JMenuItem("保存(S)");  
        Save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,InputEvent.CTRL_MASK));  
        Save.addActionListener(this);  
            //创建“另存为”并注册监视器和快捷键
        SaveAs=new JMenuItem("另存为(A)...");  
        SaveAs.addActionListener(this);  
            //创建“退出”并注册监视器
        Exit=new JMenuItem("退出(X)");  
        Exit.addActionListener(this); 
        
        //向菜单条添加"文件"菜单及菜单项  
        menuBar.add(fileMenu);   
        fileMenu.add(New);   
        fileMenu.add(Open);   
        fileMenu.add(Save);   
        fileMenu.add(SaveAs);   
        fileMenu.addSeparator();   //分隔线    
        fileMenu.add(Exit);  
        
 //编辑 
         //创建“编辑”菜单并设置快捷键ALT+E
        editMenu=new JMenu("编辑(E)");  
        editMenu.setMnemonic('E');//  
        editMenu.addMenuListener(new MenuListener()  {
           public void menuCanceled(MenuEvent e){    //取消菜单时调用  
              checkMenuItemEnabled();    //设置剪切、复制、粘贴、删除等功能的可用性  
            }  
            public void menuDeselected(MenuEvent e){    //取消选择某个菜单时调用  
               checkMenuItemEnabled();     
            }  
            public void menuSelected(MenuEvent e){     //选择某个菜单时调用  
              checkMenuItemEnabled();    
            }  
        });  
            //创建“复制”并注册监视器和快捷键
        Copy=new JMenuItem("复制(C)");  
        Copy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,InputEvent.CTRL_MASK));  
        Copy.addActionListener(this); 
            //创建“粘贴”并注册监视器和快捷键
        Paste=new JMenuItem("粘贴(P)");  
        Paste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,InputEvent.CTRL_MASK));  
        Paste.addActionListener(this);  
            //创建“剪切”并注册监视器和快捷键
        Cut=new JMenuItem("剪切(T)");  
        Cut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,InputEvent.CTRL_MASK));  
        Cut.addActionListener(this);
            //创建“查找”并注册监视器和快捷键
        Find=new JMenuItem("查找(F)...");  
        Find.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F,InputEvent.CTRL_MASK));  
        Find.addActionListener(this);  
            //创建“撤销”并注册监视器和快捷键
        Undo=new JMenuItem("撤销(U)");  
        Undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,InputEvent.CTRL_MASK));  
        Undo.addActionListener(this);  
        Undo.setEnabled(false); 
            //创建“删除”并注册监视器和快捷键
        Delete=new JMenuItem("删除(D)");  
        Delete.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE,0));  
        Delete.addActionListener(this); 
        
        //向菜单条添加"编辑"菜单及菜单项 
        menuBar.add(editMenu); 
        editMenu.add(Copy);
        editMenu.add(Paste);
        editMenu.add(Cut);
        editMenu.add(Delete);
        editMenu.addSeparator();        //分隔线 
        editMenu.add(Find);
        editMenu.addSeparator();        //分隔线  
        editMenu.add(Undo);
//格式      
        //创建“格式”菜单及菜单项并注册事件监听
        formatMenu=new JMenu("格式(O)");   
        formatMenu.setMnemonic('O');//设置快捷键ALT+O  
            //创建“自动换行”并注册监视器和快捷键
        LineWrap=new JCheckBoxMenuItem("自动换行(W)");  
        LineWrap.setMnemonic('W'); //设置快捷键ALT+W  
        LineWrap.setState(true);  
        LineWrap.addActionListener(this);
        
        //向菜单条添加"格式"菜单及菜单项
        menuBar.add(formatMenu); 
        formatMenu.add(LineWrap); 
    
//查看      
        //创建“查看”菜单及菜单项并注册事件监听  
        viewMenu=new JMenu("查看(V)");  
        viewMenu.setMnemonic('V');//设置快捷键ALT+V  
            //创建“状态栏”并注册监视器
        Status=new JCheckBoxMenuItem("状态栏(S)");  
        Status.setMnemonic('S');//设置快捷键ALT+S  
        Status.setState(true);  
        Status.addActionListener(this); 
        
        //向菜单条添加"查看"菜单及菜单项   
        menuBar.add(viewMenu);   
        viewMenu.add(Status);  
        
//帮助        
        //创建“帮助”菜单及菜单项并注册事件监听  
        helpMenu = new JMenu("帮助(H)");  
        helpMenu.setMnemonic('H');//设置快捷键ALT+H  
            //创建“查看帮助”并注册监视器和快捷键
        HelpTopics = new JMenuItem("查看帮助(H)");   
        HelpTopics.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1,0));  
        HelpTopics.addActionListener(this);  
            //创建“关于”并注册监视器和快捷键
        AboutNotepad = new JMenuItem("关于(A)");   
        AboutNotepad.addActionListener(this);  
  
        //向菜单条添加"帮助"菜单及菜单项  
        menuBar.add(helpMenu);  
        helpMenu.add(HelpTopics);  
        helpMenu.addSeparator();  //分割线
        helpMenu.add(AboutNotepad); 
       
//窗口               
        this.setJMenuBar(menuBar);
        

 //右键    
            //创建
        popupMenu=new JPopupMenu(); 
        popupMenu_Copy=new JMenuItem("复制(C)");  
        popupMenu_Paste=new JMenuItem("粘帖(P)");    
        popupMenu_Cut=new JMenuItem("剪切(T)"); 
        popupMenu_Undo=new JMenuItem("撤销(U)");
        popupMenu_Delete=new JMenuItem("删除(D)");
           //向右键菜单添加菜单项和分隔符  
        popupMenu.add(popupMenu_Copy);
        popupMenu.add(popupMenu_Paste);
        popupMenu.add(popupMenu_Cut);
        popupMenu.addSeparator();        //分隔线 
        popupMenu.add(popupMenu_Undo);
        popupMenu.add(popupMenu_Delete);
            //文本编辑区注册右键菜单事件
        popupMenu_Copy.addActionListener(this);
        popupMenu_Paste.addActionListener(this);
        popupMenu_Cut.addActionListener(this);
        popupMenu_Undo.addActionListener(this);
        popupMenu_Delete.addActionListener(this);
        
//文本区     
        editArea=new JTextArea(20,50);  
        JScrollPane scroller=new JScrollPane(editArea);  //设置滚动条
        scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); 
        editArea.setWrapStyleWord(true);  //设置单词在一行不足容纳时换行  
        editArea.setLineWrap(true);       //设置文本编辑区自动换行默认为true,即会"自动换行"
        this.add(scroller,BorderLayout.CENTER);
        oldValue=editArea.getText(); 
        editArea.getDocument().addUndoableEditListener(undoHandler);  
        editArea.getDocument().addDocumentListener(this);  
  
        //文本编辑区注册右键菜单事件  
        editArea.addMouseListener(new MouseAdapter()  
        {   public void mousePressed(MouseEvent e)  {  
             if(e.isPopupTrigger()) {  //返回此鼠标事件是否为该平台的弹出菜单触发事件  
                popupMenu.show(e.getComponent(),e.getX(),e.getY());//在组件调用者的坐标空间中的位置 X、Y 显示弹出菜单  
                }  
                checkMenuItemEnabled(); 
                editArea.requestFocus();  
            }  
            public void mouseReleased(MouseEvent e) {  
              if(e.isPopupTrigger()) {   
                 popupMenu.show(e.getComponent(),e.getX(),e.getY());// 
                }  
                checkMenuItemEnabled();
                editArea.requestFocus();  
            }  
        });
        //文本编辑区注册右键菜单事件结束 

//窗口
        this.setLocation(100,100); //位置 
        this.setSize(750,600);  //大小
        this.setVisible(true); // 可见性
            //窗口监听器  
        addWindowListener(new WindowAdapter() { 
           public void windowClosing(WindowEvent e) { 
               exitWindowChoose();  
            }
        });  
      
        checkMenuItemEnabled();  
        editArea.requestFocus();  
        
//状态栏  
        statusLabel=new JLabel("按F1获取帮助");  
        this.add(statusLabel,BorderLayout.SOUTH);//向窗口添加状态栏标签
        
    }
	//构造函数Mynote结束 
	
//撤销操作
	protected UndoManager undo=new UndoManager(); 
	protected UndoableEditListener undoHandler=new UndoHandler();
	
//设置剪切，复制，粘帖、删除功能
	public void checkMenuItemEnabled() {
		String selectText=editArea.getSelectedText();  
        if(selectText!=null)   { 
            Copy.setEnabled(true);  
            popupMenu_Copy.setEnabled(true);  
            Delete.setEnabled(true); 
        	Cut.setEnabled(true);  
            popupMenu_Cut.setEnabled(true); 
            popupMenu_Delete.setEnabled(true); 
        }  
        else   {
        	Copy.setEnabled(false);  
            popupMenu_Copy.setEnabled(false); 
        	Cut.setEnabled(false);  
            popupMenu_Cut.setEnabled(false);   
            Delete.setEnabled(false);  
            popupMenu_Delete.setEnabled(false);   
        }   
        Transferable contents=clipBoard.getContents(this);  
        if(contents!=null) {  
            Paste.setEnabled(true);  
            popupMenu_Paste.setEnabled(true); 
        }  
        else  { 
        	Paste.setEnabled(false);  
            popupMenu_Paste.setEnabled(false); 
        }  
    }
	//方法checkMenuItemEnabled()结束  

//查找 
    public void find()  { 
        final JDialog findDialog=new JDialog(this,"查找",false);//false时允许其他窗口同时处于激活状态 
        Container con=findDialog.getContentPane();//返回此对话框      
        con.setLayout(new FlowLayout(FlowLayout.LEFT));  
        JLabel findContentLabel=new JLabel("查找内容(N)：");  
        final JTextField findText=new JTextField(15);   
        final JCheckBox matchCheckBox=new JCheckBox("区分大小写(C)");  
        ButtonGroup bGroup=new ButtonGroup();  
        final JRadioButton upButton=new JRadioButton("向上(U)");  
        final JRadioButton downButton=new JRadioButton("向下(U)");  
        downButton.setSelected(true);  
        bGroup.add(upButton);  
        bGroup.add(downButton);
        JButton cancel=new JButton("取消");   //创建“取消”按钮
        //取消按钮事件处理
        cancel.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) { 
                findDialog.dispose();  
            }  
        });  
        //创建"查找"对话框的界面  
        JPanel panel1=new JPanel();  
        JPanel panel2=new JPanel();  
        JPanel panel3=new JPanel();  
        JPanel directionPanel=new JPanel();  
        directionPanel.setBorder(BorderFactory.createTitledBorder("方向"));  
        //设置directionPanel组件的边框;  
        directionPanel.add(upButton);  
        directionPanel.add(downButton);  
        panel1.setLayout(new GridLayout(2,1));  
        panel1.add(cancel);  
        panel2.add(findContentLabel);  
        panel2.add(findText);  
        panel2.add(panel1);  
        panel3.add(matchCheckBox);  
        panel3.add(directionPanel);  
        con.add(panel2);  
        con.add(panel3);  
        findDialog.setSize(410,180);  
        findDialog.setResizable(false);//不可调整大小  
        findDialog.setLocation(230,280);  
        findDialog.setVisible(true);    
    } //查找方法结束  
    
//关闭\退出窗口  
    public void exitWindowChoose()  {  
    	editArea.requestFocus();  
        String currentValue=editArea.getText();  
        if(currentValue.equals(oldValue)==true)  { 
           System.exit(0);  
        }  
        else  {
        	//假如文件未保存退出
            int exitChoose=JOptionPane.showConfirmDialog(this,"您的文件尚未保存，是否保存？","退出提示",JOptionPane.YES_NO_CANCEL_OPTION);  
            if(exitChoose==JOptionPane.YES_OPTION)  {  
            	boolean isSave=false; 
                if(isNewFile)   { 
                    String str=null;  
                    JFileChooser fileChooser=new JFileChooser();  
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);  
                    fileChooser.setApproveButtonText("确定");  
                    fileChooser.setDialogTitle("另存为");  
                    int result=fileChooser.showSaveDialog(this); 
                    if(result==JFileChooser.CANCEL_OPTION)  { 
                        statusLabel.setText("您没有保存文件");  
                        return;  
                    }   
                    File saveFileName=fileChooser.getSelectedFile();  
                    //假如命名错误
                    if(saveFileName==null||saveFileName.getName().equals("")) {  
                        JOptionPane.showMessageDialog(this,"不合法的文件名","不合法的文件名",JOptionPane.ERROR_MESSAGE);  
                    }  
                    else   { 
                        try  {
                            FileWriter fw=new FileWriter(saveFileName);  
                            BufferedWriter bfw=new BufferedWriter(fw);  
                            bfw.write(editArea.getText(),0,editArea.getText().length());  
                            bfw.flush();  
                            fw.close();  
                            isNewFile=false;  
                            currentFile=saveFileName;  
                            oldValue=editArea.getText();  
                            this.setTitle(saveFileName.getName()+"  - 记事本");  
                            statusLabel.setText("当前打开文件:"+saveFileName.getAbsoluteFile());  
                            isSave=true;  
                        }                             
                        catch(IOException ioException){                   
                        }                 
                    }  
                }  
                else   { 
                    try  { 
                        FileWriter fw=new FileWriter(currentFile);  
                        BufferedWriter bfw=new BufferedWriter(fw);  
                        bfw.write(editArea.getText(),0,editArea.getText().length());  
                        bfw.flush();  
                        fw.close();  
                        isSave=true;  
                    }                             
                    catch(IOException ioException){                   
                    }  
                }  
                System.exit(0);  
                if(isSave)
                	System.exit(0);  
                else 
                	return;  
            }  
            else if(exitChoose==JOptionPane.NO_OPTION)  
                System.exit(0);  
            else  
               return;  
        }  
    }//关闭窗口时调用方法结束
    
       
//新建
    public void actionPerformed(ActionEvent e)   {
        if(e.getSource()==New) { 
           editArea.requestFocus();  
            String currentValue=editArea.getText();  
            boolean isTextChange=(currentValue.equals(oldValue))?false:true;  
            if(isTextChange){  
                int saveChoose=JOptionPane.showConfirmDialog(this,"您的文件尚未保存，是否保存？","提示",JOptionPane.YES_NO_CANCEL_OPTION);  
                if(saveChoose==JOptionPane.YES_OPTION) { 
                    String str=null;  
                    JFileChooser fileChooser=new JFileChooser();  
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);  
                    fileChooser.setApproveButtonText("确定");  
                    fileChooser.setDialogTitle("另存为");  
                    int result=fileChooser.showSaveDialog(this);  
                    if(result==JFileChooser.CANCEL_OPTION) {
                        statusLabel.setText("您没有选择任何文件");  
                        return;  
                    }  
                    File saveFileName=fileChooser.getSelectedFile();  
                    if(saveFileName==null || saveFileName.getName().equals("")){  
                       JOptionPane.showMessageDialog(this,"不合法的文件名","不合法的文件名",JOptionPane.ERROR_MESSAGE);  
                    }  
                    else {  
                        try {  
                            FileWriter fw=new FileWriter(saveFileName);  
                            BufferedWriter bfw=new BufferedWriter(fw);  
                            bfw.write(editArea.getText(),0,editArea.getText().length());  
                            bfw.flush();//刷新该流的缓冲  
                            bfw.close();  
                            isNewFile=false;  
                            currentFile=saveFileName;  
                            oldValue=editArea.getText();  
                            this.setTitle(saveFileName.getName()+" - 记事本");  
                            statusLabel.setText("当前打开文件："+saveFileName.getAbsoluteFile());  
                        }  
                        catch (IOException ioException) { 
                          
                        }  
                    }  
                }  
                else if(saveChoose==JOptionPane.NO_OPTION) { 
                    editArea.replaceRange("",0,editArea.getText().length());  
                    statusLabel.setText(" 新建文件");  
                    this.setTitle("无标题 - 记事本");  
                    isNewFile=true;  
                    undo.discardAllEdits(); //撤消所有的"撤消"操作  
                    Undo.setEnabled(false);  
                    oldValue=editArea.getText();  
                }  
                else if(saveChoose==JOptionPane.CANCEL_OPTION) {  
                    return;  
                }  
            }  
            else  {
                editArea.replaceRange("",0,editArea.getText().length());  
                statusLabel.setText(" 新建文件");  
                this.setTitle("我的记事本");  
                isNewFile=true;  
                undo.discardAllEdits();
                Undo.setEnabled(false);  
                oldValue=editArea.getText();  
            }  
        }//新建结束 
        
//打开  
        else if(e.getSource()==Open) { 
            editArea.requestFocus();  
            String currentValue=editArea.getText();  
            boolean isTextChange=(currentValue.equals(oldValue))?false:true;  
            if(isTextChange)  {
                int saveChoose=JOptionPane.showConfirmDialog(this,"您的文件尚未保存，是否保存？","提示",JOptionPane.YES_NO_CANCEL_OPTION);  
                if(saveChoose==JOptionPane.YES_OPTION) { 
                    String str=null;  
                    JFileChooser fileChooser=new JFileChooser();  
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);  
                    fileChooser.setApproveButtonText("确定");  
                    fileChooser.setDialogTitle("另存为");  
                    int result=fileChooser.showSaveDialog(this);  
                    if(result==JFileChooser.CANCEL_OPTION){  
                        statusLabel.setText("您没有选择任何文件");  
                        return;  
                    }  
                    File saveFileName=fileChooser.getSelectedFile();  
                    if(saveFileName==null || saveFileName.getName().equals("")){  
                        JOptionPane.showMessageDialog(this,"不合法的文件名","不合法的文件名",JOptionPane.ERROR_MESSAGE);  
                    }  
                    else {  
                        try  {
                            FileWriter fw=new FileWriter(saveFileName);  
                            BufferedWriter bfw=new BufferedWriter(fw);  
                            bfw.write(editArea.getText(),0,editArea.getText().length());  
                            bfw.flush();//刷新该流的缓冲  
                            bfw.close();  
                            isNewFile=false;  
                            currentFile=saveFileName;  
                            oldValue=editArea.getText();  
                            this.setTitle(saveFileName.getName()+" - 记事本");  
                            statusLabel.setText("当前打开文件："+saveFileName.getAbsoluteFile());  
                        }  
                        catch (IOException ioException){ }  
                    }  
                }  
                else if(saveChoose==JOptionPane.NO_OPTION) { 
                    String str=null;  
                    JFileChooser fileChooser=new JFileChooser();  
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);  
                    fileChooser.setApproveButtonText("确定");  
                    fileChooser.setDialogTitle("打开文件");  
                    int result=fileChooser.showOpenDialog(this);  
                    if(result==JFileChooser.CANCEL_OPTION)  {
                        statusLabel.setText("您没有选择任何文件");  
                        return;  
                    }  
                    File fileName=fileChooser.getSelectedFile();  
                    if(fileName==null || fileName.getName().equals("")) { 
                        JOptionPane.showMessageDialog(this,"不合法的文件名","不合法的文件名",JOptionPane.ERROR_MESSAGE);  
                    }  
                    else   {
                        try { 
                            FileReader fr=new FileReader(fileName);  
                            BufferedReader bfr=new BufferedReader(fr);  
                            editArea.setText("");  
                            while((str=bfr.readLine())!=null) { 
                                editArea.append(str);  
                            }  
                            this.setTitle(fileName.getName()+" - 记事本");  
                            statusLabel.setText(" 当前打开文件："+fileName.getAbsoluteFile());  
                            fr.close();  
                            isNewFile=false;  
                            currentFile=fileName;  
                            oldValue=editArea.getText();  
                        }  
                        catch (IOException ioException)  { } 
                    }  
                }  
                else   {
                    return;  
                }  
            }  
            else  {
                String str=null;  
                JFileChooser fileChooser=new JFileChooser();  
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);  
                fileChooser.setApproveButtonText("确定");  
                fileChooser.setDialogTitle("打开文件");  
                int result=fileChooser.showOpenDialog(this);  
                if(result==JFileChooser.CANCEL_OPTION)  { 
                    statusLabel.setText(" 您没有选择任何文件 ");  
                    return;  
                }  
                File fileName=fileChooser.getSelectedFile();  
                if(fileName==null || fileName.getName().equals("")) { 
                    JOptionPane.showMessageDialog(this,"不合法的文件名","不合法的文件名",JOptionPane.ERROR_MESSAGE);  
                }  
                else { 
                    try  {
                        FileReader fr=new FileReader(fileName);  
                        BufferedReader bfr=new BufferedReader(fr);  
                        editArea.setText("");  
                        while((str=bfr.readLine())!=null){  
                            editArea.append(str);  
                        }  
                        this.setTitle(fileName.getName()+" - 记事本");  
                        statusLabel.setText(" 当前打开文件："+fileName.getAbsoluteFile());  
                        fr.close();  
                        isNewFile=false;  
                        currentFile=fileName;  
                        oldValue=editArea.getText();  
                    }  
                    catch (IOException ioException)  {   } 
                }  
            }  
        }
        //打开结束  
//保存  
        else if(e.getSource()==Save)  {
            editArea.requestFocus();  
            if(isNewFile)  {
                String str=null;  
                JFileChooser fileChooser=new JFileChooser();  
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);  
                fileChooser.setApproveButtonText("确定");  
                fileChooser.setDialogTitle("保存");  
                int result=fileChooser.showSaveDialog(this);  
                if(result==JFileChooser.CANCEL_OPTION) {  
                    statusLabel.setText("您没有选择任何文件");  
                    return;  
                }  
                File saveFileName=fileChooser.getSelectedFile();  
                if(saveFileName==null || saveFileName.getName().equals("")){  
                    JOptionPane.showMessageDialog(this,"不合法的文件名","不合法的文件名",JOptionPane.ERROR_MESSAGE);  
                }  
                else  {  
                    try { 
                        FileWriter fw=new FileWriter(saveFileName);  
                        BufferedWriter bfw=new BufferedWriter(fw);  
                        bfw.write(editArea.getText(),0,editArea.getText().length());  
                        bfw.flush();  
                        bfw.close();  
                        isNewFile=false;  
                        currentFile=saveFileName;  
                        oldValue=editArea.getText();  
                        this.setTitle(saveFileName.getName()+" - 记事本");  
                        statusLabel.setText("当前打开文件："+saveFileName.getAbsoluteFile());  
                    }  
                    catch (IOException ioException)  {  } 
                }  
            }  
            else { 
                try {
                     FileWriter fw=new FileWriter(currentFile);  
                    BufferedWriter bfw=new BufferedWriter(fw);  
                    bfw.write(editArea.getText(),0,editArea.getText().length());  
                    bfw.flush();  
                    fw.close();  
                }                             
                catch (IOException ioException)  {  } 
            }  
        }
            //保存结束  
//另存为  
        else if(e.getSource()==SaveAs) {  
            editArea.requestFocus();  
            String str=null;  
            JFileChooser fileChooser=new JFileChooser();  
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);  
            fileChooser.setApproveButtonText("确定");  
            fileChooser.setDialogTitle("另存为");  
            int result=fileChooser.showSaveDialog(this);  
            if(result==JFileChooser.CANCEL_OPTION) { 
                statusLabel.setText("您没有选择任何文件");  
                return;  
            }                 
            File saveFileName=fileChooser.getSelectedFile();  
            if(saveFileName==null||saveFileName.getName().equals("")) {  
                JOptionPane.showMessageDialog(this,"不合法的文件名","不合法的文件名",JOptionPane.ERROR_MESSAGE);  
            }     
            else  {  
                try { 
                    FileWriter fw=new FileWriter(saveFileName);  
                    BufferedWriter bfw=new BufferedWriter(fw);  
                    bfw.write(editArea.getText(),0,editArea.getText().length());  
                    bfw.flush();  
                    fw.close();  
                    oldValue=editArea.getText();  
                    this.setTitle(saveFileName.getName()+" - 记事本");  
                    statusLabel.setText("当前打开文件:"+saveFileName.getAbsoluteFile());  
                }                         
                catch (IOException ioException)  {  }           
            }  
        }
        //另存为结束
//退出  
        else if(e.getSource()==Exit)  {
            int exitChoose=JOptionPane.showConfirmDialog(this,"确定要退出吗?","退出提示",JOptionPane.OK_CANCEL_OPTION);  
            if(exitChoose==JOptionPane.OK_OPTION) { 
                System.exit(0);  
            }  
            else { 
                return;  
            }  
        }
        //退出结束  

//复制  
        else if(e.getSource()==Copy || e.getSource()==popupMenu_Copy) { 
            checkMenuItemEnabled();  
            editArea.requestFocus();  
            String text=editArea.getSelectedText();  
            StringSelection selection=new StringSelection(text);  
            clipBoard.setContents(selection,null); 
        }
        //复制结束  
//粘帖  
        else if(e.getSource()==Paste || e.getSource()==popupMenu_Paste) { 
            editArea.requestFocus();  
            Transferable contents=clipBoard.getContents(this);  
            if(contents==null)return;  
            String text="";  
            try { 
                text=(String)contents.getTransferData(DataFlavor.stringFlavor);  
            }  
            catch (Exception exception) { }
            editArea.replaceRange(text,editArea.getSelectionStart(),editArea.getSelectionEnd());  
            checkMenuItemEnabled();  
        }
        //粘帖结束  
        
//撤销  
        else if(e.getSource()==Undo || e.getSource()==popupMenu_Undo) { 
           editArea.requestFocus();  
            if(undo.canUndo()){  
               try { 
                    undo.undo();  
                }  
                catch (CannotUndoException ex) { 
                    System.out.println("Unable to undo:" + ex);  
                    ex.printStackTrace();  
                }  
            }  
            if(!undo.canUndo()){  
                   Undo.setEnabled(false);  
                }  
        }
        //撤销结束
//剪切  
        else if(e.getSource()==Cut || e.getSource()==popupMenu_Cut) {   
            checkMenuItemEnabled();
            editArea.requestFocus();  
            String text=editArea.getSelectedText();  
            StringSelection selection=new StringSelection(text);  
            clipBoard.setContents(selection,null);  
            editArea.replaceRange("",editArea.getSelectionStart(),editArea.getSelectionEnd());
        }
        //剪切结束  
//删除
        else if(e.getSource()==Delete || e.getSource()==popupMenu_Delete) {
            checkMenuItemEnabled();  
            editArea.requestFocus();  
            editArea.replaceRange("",editArea.getSelectionStart(),editArea.getSelectionEnd());  
        }
//查找  
        else if(e.getSource()==Find)  {
           editArea.requestFocus();  
            find();  
        }
        //查找结束  
//自动换行  
        else if(e.getSource()==LineWrap){  
            if(LineWrap.getState())  
                editArea.setLineWrap(true);  
            else   
                editArea.setLineWrap(false); 
        }
        //自动换行结束  
//设置状态栏可见性  
        else if(e.getSource()==Status) {  
            if(Status.getState())  
                statusLabel.setVisible(true);  
            else   
                statusLabel.setVisible(false);  
        }
        //设置状态栏可见性结束 
//查看/帮助 
        else if(e.getSource()==HelpTopics)  {
            editArea.requestFocus();  
            JOptionPane.showMessageDialog(this,"啥也不是","帮助主题",JOptionPane.INFORMATION_MESSAGE);  
        }//查看帮助结束  
//关于  
        else if(e.getSource()==AboutNotepad) {
            editArea.requestFocus();  
            JOptionPane.showMessageDialog(this,  
                "java课程设计实验\n"+  
                " 编写者：0224杨文舒"+  
                " 编写时间：2020-6-17                          \n"+  
                " e-mail：710898670@qq.com\n"+  
                " 一些地方借鉴他人，不足之处希望大家能提出意见，谢谢！  \n",  
                "记事本",JOptionPane.INFORMATION_MESSAGE);  
        }//关于结束  
    }
    //方法actionPerformed()结束  
    
    //实现DocumentListener接口中的方法(与撤销操作有关)  
    public void removeUpdate(DocumentEvent e){  
        Undo.setEnabled(true);  
    }  
    public void insertUpdate(DocumentEvent e){  
        Undo.setEnabled(true);  
    }  
    public void changedUpdate(DocumentEvent e){  
        Undo.setEnabled(true);  
    }
    //DocumentListener结束  
  
    //实现接口UndoableEditListener的类UndoHandler(与撤销操作有关)  
    class UndoHandler implements UndoableEditListener { 
         public void undoableEditHappened(UndoableEditEvent uee){  
            undo.addEdit(uee.getEdit());  
        }  
    }  
  
 }