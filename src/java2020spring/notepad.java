package java2020spring;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import javax.swing.*;
import javax.swing.plaf.FileChooserUI;
public class notepad extends JFrame implements ActionListener{
        //定义所需要的组件
    JTextArea jta=null;
    JMenuBar jmb=null;
    JMenu jm=null;
    JMenuItem jmi1=null;
    JMenuItem jmi2=null;
    public static void main(String[]args){
        new notepad();
    }
    public notepad(){
        //把组件添加到窗体上
        jta=new JTextArea();
        jmb=new JMenuBar();
        this.add(jta);
        this.setJMenuBar(jmb);
        //设置窗体属性
        this.setTitle("简易记事本");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(400, 300);
        this.setVisible(true);
        //把菜单添加进菜单条
        jm=new JMenu("文件");
        jm.setMnemonic('f');
        jmb.add(jm);
        //把子菜单加入至菜单
        jmi1=new JMenuItem("打开");
        jmi2=new JMenuItem("保存");
        jmi1.setActionCommand("open");
        jmi2.setActionCommand("save");
        jm.add(jmi1);
        jm.add(jmi2);
        //为两个字菜单注册事件监听
        jmi1.addActionListener(this);
        jmi2.addActionListener(this);
    }
    public void actionPerformed(ActionEvent e){
        //对点击了打开进行处理
        if(e.getActionCommand().equals("open")){
              //创建一个文件选择组件
            JFileChooser jfc1=new JFileChooser();
            //设置文件选择器的名字
            jfc1.setDialogTitle("请选择文件....");
            //设置文件选择组件的类型(打开类型)
            jfc1.showOpenDialog(null);
            //显示该组件
            jfc1.setVisible(true);
            //获取选择文件的绝对路径
            String s;
            s=jfc1.getSelectedFile().getAbsolutePath();
            //将该文件显示到记事本上
            BufferedReader br=null;
            FileReader fr=null;
            try{
                //创建一个带缓冲的文件读取对象
                fr=new FileReader(s);
                br=new BufferedReader(fr);
                String text="";
                String m=null;
                //循环读取文件
                while((m=br.readLine())!=null){
                    text+=m+"\r\n";
                }
                //将读取的结果打印到记事本上面
                this.jta.setText(text);
            }catch(Exception e1){
                e1.printStackTrace();
            }
            finally{
                  //关掉打开的文件
                try{
                br.close();
                fr.close();
            }catch(Exception e2){
                e2.printStackTrace();
            }
        }
        }
        else if(e.getActionCommand().equals("save")){
            //创建一个文件选择组件
            JFileChooser jfc=new JFileChooser();
            //设置文件选择的名称
            jfc.setDialogTitle("另存为");
            //设置文件选择组件的类型（保存类型）
            jfc.showSaveDialog(null);
            //显示该组件
            jfc.setVisible(true);
            //获取选择文件的绝对路径
            String filename;
            filename=jfc.getSelectedFile().getAbsolutePath();
            //将记事本内的文本保存至该路径
            BufferedWriter bw=null;
            FileWriter fw=null;
            try{
                //创建文件输出文件
                fw=new FileWriter(filename);
                bw=new BufferedWriter(fw);
                //获取文本
                String outtext="";
                outtext=this.jta.getText();
                //输出文本
                fw.write(outtext);
                
            }catch(Exception e2){
                e2.printStackTrace();
            }
            finally{
                //关闭打开的输出文件
                try{
                    bw.close();
                    fw.close();
                }catch(Exception e3){
                    e3.printStackTrace();
                }
            }
        }
    }
}